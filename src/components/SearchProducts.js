import React, { useState } from 'react'
import { SEARCH_BAR_PLACEHOLDER } from '../assets/text/en_AU'
import { FaSearch } from 'react-icons/fa'
import { ImCross } from 'react-icons/im'
import { useDispatch } from 'react-redux'
import { searchProducts } from '../actions/index'

//Handles search product logic

export const SearchProducts = ({hideIconTitle}) => {
    const dispatch = useDispatch()
    const [searchBoxVisibility, setSearchBoxVisibility] = useState(false)
    const [searchString, setSearchString] = useState("")


    const handleChange = event => {
        setSearchString(event.target.value)
        dispatch(searchProducts(searchString))
    };

    const handleSubmit = event => {
        event.preventDefault()
        dispatch(searchProducts(searchString))
    };

    const onResetProductSearch = () => {
        setSearchString('')
        setSearchBoxVisibility(false)
        dispatch(searchProducts(''))
        hideIconTitle(false)
    };

    const onProductSearch = () => {
        setSearchBoxVisibility(true)
        hideIconTitle(true)
    };

    return (
            <form  onSubmit={handleSubmit}>
                {searchBoxVisibility ? 
                <>
                    <ImCross color= "white" onClick={onResetProductSearch}></ImCross>
                    <input type="text" placeholder={SEARCH_BAR_PLACEHOLDER} value={searchString} onChange={handleChange}/>    
                </>:
                    <FaSearch color= "white" onClick={onProductSearch}></FaSearch>
                }
            </form>
    )
}