import React, { memo } from 'react'

//Rendering Products
export const ProductGallery = ({productArray}) => {
    return (
        <div className="products-container">  
            {(productArray || []).map((product, i) =>
            <div className="product-container" key={i}>
                    <img className="image" 
                            src={product.imageURL} 
                            alt={product.name}/>
                    <div className="product-text">
                        <span>{product.brand}</span>
                        <span>{product.name}</span>
                        <span style={{fontWeight:"bold"}}>${product.price}</span>
                    </div>
                  </div>)}
       </div>
      )
}
export const ProductGalleryMemo = memo(ProductGallery)