import React, { useState } from 'react'
import { MAIN_TITLE } from '../assets/text/en_AU'
import { SearchProducts } from './SearchProducts'


export const Heading = () => {
    
    const [hideIconTitle, setHideIconTitle] = useState(false);
    
    return (
        <div className='header'>
            {!hideIconTitle && <p className="title-heading">{MAIN_TITLE}</p>}
            <div className={!hideIconTitle ? 'search-box-hidden' : 'search-box-visible'}>
                <SearchProducts hideIconTitle={setHideIconTitle}></SearchProducts>
            </div>
        </div>
    )
}