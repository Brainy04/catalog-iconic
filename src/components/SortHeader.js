import { SortProducts } from './SortProducts'

export const SortHeader = ({searchedProductsLength}) => {
    return (
        <div className="sort-header">
            <span>{searchedProductsLength} items found</span>
            <SortProducts></SortProducts>
        </div>
    )
}