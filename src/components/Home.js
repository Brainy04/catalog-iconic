import { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import Loader from 'react-loader-spinner'
import { fetchProducts } from '../actions/index'
import { SortHeader } from './SortHeader'
import { ProductGalleryMemo } from './ProductGallery'
import { Heading } from './Heading'
import { getSearchedProducts } from '../selectors/index'
import { ERROR_MESSAGE_TITLE } from '../assets/text/en_AU'
import Pagination from './Pagination'

export const Home = () => {
  const dispatch = useDispatch()
  const searchedProducts = useSelector(getSearchedProducts)
  const errorMsg = useSelector(state => state.productData.errorMsg)
  const isFetching = useSelector(state => state.productData.isFetching)

  const [currentPage, setCurrentPage] = useState(1);
  const [productsPerPage] = useState(5);
  
    //Initial Load
    useEffect(() => {
      const getProducts = async() => {
        dispatch(fetchProducts())
      }
      getProducts()
    },[])
    
    useEffect(() => {
     if(searchedProducts.length > 6) {
      setCurrentPage(1)
     }
    },[searchedProducts])

    // Get current products
    const indexOfLastPost = currentPage * productsPerPage;
    const indexOfFirstPost = indexOfLastPost - productsPerPage;
    const currentProducts = searchedProducts.slice(indexOfFirstPost, indexOfLastPost);

     // Change page
     const paginate = pageNumber => setCurrentPage(pageNumber);

    return (
      <div className="main-container">
        <Heading/>
        {isFetching ? 
            <Loader type="Circles" color="black" height={50} width={50} className='align-center'/>:
            <>
              <SortHeader searchedProductsLength={searchedProducts.length}></SortHeader>
              <Pagination
                productsPerPage={productsPerPage}
                totalProducts={searchedProducts.length}
                paginate={paginate}
                currentPage = {currentPage}
              />
              <ProductGalleryMemo productArray={currentProducts}></ProductGalleryMemo>
              {(errorMsg) ? <div className="error-block">
                                <p>{ERROR_MESSAGE_TITLE}</p> 
                                <p>Reason: {errorMsg}</p>
                              </div> : null}
              
            </>
        }
      </div>
    )
}