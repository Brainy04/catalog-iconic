import { useDispatch } from 'react-redux'
import { FILTER_CRITERIA } from '../assets/text/en_AU'
import { sortProducts } from '../actions/index'

//Handles the sort dropdown logic
export const SortProducts = () => {
    const dispatch = useDispatch()

    const handleChange = event => {
      event.preventDefault()
      const selectedSort = event.target.value && event.target.value.toLowerCase()
      dispatch(sortProducts(selectedSort))
    }

    return (
        <>
          {FILTER_CRITERIA && FILTER_CRITERIA.length > 0 && (
            <div className="sort-products">
              <select onChange={handleChange}>
                {FILTER_CRITERIA.map((criteria, index) => {
                  return <option key={index} value={criteria}>{criteria}</option>
                })}
              </select>
            </div>
          )}
        </>
    )
}