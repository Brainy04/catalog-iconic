import * as ACTIONS from '../actions/actionTypes';
const defaultState = {
  isFetching: false,
  productArray: [],
  errorMsg:'',
  currentSort: 'price high to low',
  searchString: ''
}
const productData = (state = defaultState, action) => {
  switch (action.type) {
    case ACTIONS.REQUEST_PRODUCTS:
      return {
        ...state,
          isFetching: true,
          productArray: [],
          errorMsg:''
      }
    case ACTIONS.RECEIVED_PRODUCTS:
      const mapProducts = action.productsData.map(item => {
        return {
            imageURL: item._embedded.images[0].url,
            name: item.name,
            brand: item._embedded.brand.name,
            price: item.price,
        }
      })
      return {
          ...state,
          isFetching: false,
          productArray: [...state.productArray, ...mapProducts],
          errorMsg:''
        }
    case ACTIONS.ERROR_RECEIVED:
    return {
        ...state,
        isFetching: false,
        productArray: [],
        errorMsg: action.errorMessage
      }
    case ACTIONS.SEARCH_PRODUCTS:
      return {
          ...state,
          searchString: action.searchString
      };
    case ACTIONS.SORT_PRODUCTS:
        let sortedArray = [];
        switch(action.currentSort.toLowerCase()) {
          case 'price high to low': 
              sortedArray = state.productArray.sort((a, b) =>  b.price - a.price);
            break;
          case 'price low to high': 
            sortedArray = state.productArray.sort((a, b) =>  a.price - b.price);
          break;
          case 'brand a to z':
              sortedArray = state.productArray.sort((a, b) => a.brand.localeCompare(b.brand));
            break;
          case 'brand z to a':
              sortedArray = state.productArray.sort((a, b) => b.brand.localeCompare(a.brand));
              break;
          default:
        }
      return {
          ...state,
          currentSort: action.currentSort,
          productArray: [...sortedArray]
        };
    default:
      return state
  }
}

export default productData