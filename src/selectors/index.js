import { createSelector } from 'reselect'
 
const getProductArray = (state) => state.productData.productArray
const searchString = (state) => state.productData.searchString

//To get the searched products list
export const getSearchedProducts = createSelector(
  [ getProductArray, searchString ],
  (productArray, searchString) => {

      if(!searchString) {
        return [...productArray]
      }
      
      const searchedProducts = productArray.filter(item => {
          return (item.name && item.name.toLowerCase().includes(searchString.toLowerCase())) || (item.brand && item.brand.toLowerCase().includes(searchString.toLowerCase()))
      })
      
      return [...searchedProducts]
    }
)
