export const MAIN_TITLE = 'THE ICONIC'
export const SEARCH_BAR_PLACEHOLDER = 'Search by brand or name'
export const FILTER_CRITERIA = ['Price High to Low', 'Price Low to High', 'Brand A to Z', 'Brand Z to A']
export const API_URL = 'https://eve.theiconic.com.au/catalog/products?gender=female&page=1&page_size=10&sort=popularity'
export const ERROR_MESSAGE_TITLE = 'Something went wrong!'
