import * as ACTIONS from '../actions/actionTypes';
import  productData from '../reducers/productData';
import { responseData } from './sampleResponse'

const productArr = [{
	"imageURL": "http://static.theiconic.com.au/p/nike-0201-4972301-1.jpg",
	"name": "Nike Metcon 6 - Women's",
	"brand": "Nike",
	"price": 190
}, {
	"imageURL": "http://static.theiconic.com.au/p/nike-0233-7828801-1.jpg",
	"name": "Nike React Infinity Run - Women's",
	"brand": "Nike",
	"price": 240
}, {
	"imageURL": "http://static.theiconic.com.au/p/nike-0333-9872301-1.jpg",
	"name": "Nike Flex 2020 Run - Women's",
	"brand": "Nike",
	"price": 120
}, {
	"imageURL": "http://static.theiconic.com.au/p/nike-0139-1082301-1.jpg",
	"name": "Nike Renew In-Season TR 10 - Women's",
	"brand": "Nike",
	"price": 110
}, {
	"imageURL": "http://static.theiconic.com.au/p/eco-modern-essentials-3705-2069111-1.jpg",
	"name": "ECO. Bliss Diffuser & Best-selling Blends Collection",
	"brand": "ECO. Modern Essentials",
	"price": 130
}, {
	"imageURL": "http://static.theiconic.com.au/p/eco-modern-essentials-8726-5069111-1.jpg",
	"name": "ECO. Bliss Diffuser & Deep Sleep Trio Collection",
	"brand": "ECO. Modern Essentials",
	"price": 105
}, {
	"imageURL": "http://static.theiconic.com.au/p/camilla-4782-9107711-1.jpg",
	"name": "Oversized Tie-Dye Band Tee",
	"brand": "Camilla",
	"price": 229
}, {
	"imageURL": "http://static.theiconic.com.au/p/nike-9084-2718801-1.jpg",
	"name": "Offcourt Slides - Women's",
	"brand": "Nike",
	"price": 45
}, {
	"imageURL": "http://static.theiconic.com.au/p/nike-3702-3350301-1.jpg",
	"name": "Dri-FIT Icon Clash Training Crew",
	"brand": "Nike",
	"price": 75
}, {
	"imageURL": "http://static.theiconic.com.au/p/the-seeke-9424-3223001-1.jpg",
	"name": "Rose Quartz Face Roller",
	"brand": "The Seeke",
	"price": 49.95
}]

describe("PRODUCT DATA REDUCER", () => {

    it("REQUEST PRODUCTS", () => {

        const state = { isFetching: false,
                        productArray: [],
                        errorMsg:'',
                        currentSort: 'price high to low',
                        searchString: ''}
        
        const action = {  type: ACTIONS.REQUEST_PRODUCTS }
        const results = productData(state, action)
        expect(results).toEqual({
                                    isFetching: true,
                                    productArray: [],
                                    errorMsg:'',
                                    currentSort: 'price high to low',
                                    searchString: ''
                                }) 
        });

    it("RECEIVED PRODUCT SUCCESS", () => {

    const state = { isFetching: true,
                    productArray: [],
                    errorMsg:'',
                    currentSort: 'price high to low',
                    searchString: ''}
    
    const action = {  type: ACTIONS.RECEIVED_PRODUCTS,
                       productsData: responseData._embedded.product
                   }

    const results = productData(state, action)
    expect(results).toEqual({
                                isFetching: false,
                                productArray: productArr,
                                errorMsg:'',
                                currentSort: 'price high to low',
                                searchString: ''
                            }) 
    });

    it("RECEIVED PRODUCT ERROR", () => {
        const state = { isFetching: false,
                        productArray: productArr,
                        errorMsg:''
                      }
    
        const action = { type: ACTIONS.ERROR_RECEIVED,
                         errorMessage: 'Error in receiving products'
                       }
    
        const results = productData(state, action)
        expect(results).toEqual({
                                isFetching: false,
                                productArray: [],
                                errorMsg:'Error in receiving products'
                            }) 
                        
    });

    

    it("SEARCH PRODUCTS", () => {
    const state = { isFetching: false,
                    productArray: productArr,
                    errorMsg:'',
                    currentSort: 'price high to low',
                    searchString: ''}

    const action = { type: ACTIONS.SEARCH_PRODUCTS,
                     searchString: 'Nike React'
                   }

    const results = productData(state, action)
    expect(results).toEqual({   isFetching: false,
                                productArray: productArr,
                                errorMsg:'',
                                currentSort: 'price high to low',
                                searchString: 'Nike React'
                            }) });
    
    it("SORT PRODUCTS", () => { 
        const state = { isFetching: false,
                        productArray: productArr,
                        currentSort: 'price high to low',
                        searchString: ''}
    
        const action = { type: ACTIONS.SORT_PRODUCTS,
                         currentSort: 'brand a to z'
                       }
    
        const results = productData(state, action)

        expect(results.productArray[0].brand).toEqual('Camilla') 
    });
    
})