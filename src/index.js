import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
import { createLogger } from 'redux-logger'
import { Provider } from 'react-redux'
import rootReducer from './reducers'
import 'bootstrap/dist/css/bootstrap.css'
import App from './App';
import reportWebVitals from './reportWebVitals'


const loggerMiddleware = createLogger()
const defaultState = {}
const store = createStore(
                rootReducer,
                defaultState,
                applyMiddleware(
                  thunkMiddleware,
                  loggerMiddleware
                )
              )

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
   </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
