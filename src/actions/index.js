import * as ACTIONS from './actionTypes';
import { API_URL } from '../assets/text/en_AU'

const requestProducts = () => {
    return {
      type: ACTIONS.REQUEST_PRODUCTS
    }
}

const receivedProducts = (productsData) => {
    return {
        type: ACTIONS.RECEIVED_PRODUCTS,
        productsData: productsData
    };
};

const errorInReceivingProducts = (errorMessage) => {
    return {
      type: ACTIONS.ERROR_RECEIVED,
      errorMessage: errorMessage
    }
};

export const searchProducts = (searchString) => {
    return {
        type: ACTIONS.SEARCH_PRODUCTS,
        searchString: searchString
    }
}

export const sortProducts = (currentSort) => {
    return {
        type: ACTIONS.SORT_PRODUCTS,
        currentSort: currentSort
    }
}

export const fetchProducts = () => {
    return (dispatch) => {
      dispatch(requestProducts());
      fetch(API_URL)
      .then(response => response.json())
      .then(data => {
          dispatch(receivedProducts(data._embedded.product))
          dispatch(sortProducts('Price High to Low'));
        })
      .catch(error => {
        dispatch(errorInReceivingProducts(error.message))
        
        })
    }
  }